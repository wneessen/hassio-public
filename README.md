# My Home-Assistant projects

This repository holds some public projects of stuff I implemented into my HASS.io instance.

## Spotify Control
This project provides a "Spotify Control" box in the UI. It allows to select the output target using the "Spotify Control" capability. It also provides a volume control and some intentscripts to control the Spotify stuff via Alexa/Echo.

https://bitbucket.org/wneessen/hassio-public/src/774cfb1ca21d147dfa5614ca7d0cd13c6b4c7936/spotify-control/?at=master